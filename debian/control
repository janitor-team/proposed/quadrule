Source: quadrule
Priority: extra
Maintainer: Debian Science Maintainers <debian-science-maintainers@lists.alioth.debian.org>
Uploaders: Mike Neish <neishm@atmosp.physics.utoronto.ca>
Build-Depends: debhelper (>= 9), autotools-dev, dh-autoreconf
Standards-Version: 3.9.4
Section: math
Homepage: http://people.sc.fsu.edu/~jburkardt/c_src/quadrule/quadrule.html
Vcs-Git: git://anonscm.debian.org/debian-science/packages/quadrule.git
Vcs-Browser: http://anonscm.debian.org/gitweb/?p=debian-science/packages/quadrule.git

Package: libquadrule-dev
Section: libdevel
Architecture: any
Multi-Arch: foreign
Depends: libquadrule1 (= ${binary:Version}), ${misc:Depends}
Description: Development files for quadrule
 QUADRULE is a C library which sets up a variety of quadrature rules, used to
 approximate the integral of a function over various domains.
 .
 QUADRULE returns the abscissas and weights for a variety of one dimensional
 quadrature rules for approximating the integral of a function. The best rule is
 generally Gauss-Legendre quadrature, but other rules offer special features,
 including the ability to handle certain weight functions, to approximate an
 integral on an infinite integration region, or to estimate the approximation
 error. 
 .
 This package provides everything needed for compiling C programs that call
 quadrule functions.


Package: libquadrule1
Architecture: any
Pre-Depends: ${misc:Pre-Depends}
Depends: ${shlibs:Depends}, ${misc:Depends}
Multi-Arch: same
Description: Quadrature rules and numerical integration routines
 QUADRULE is a C library which sets up a variety of quadrature rules, used to
 approximate the integral of a function over various domains.
 .
 QUADRULE returns the abscissas and weights for a variety of one dimensional
 quadrature rules for approximating the integral of a function. The best rule is
 generally Gauss-Legendre quadrature, but other rules offer special features,
 including the ability to handle certain weight functions, to approximate an
 integral on an infinite integration region, or to estimate the approximation
 error.
 
